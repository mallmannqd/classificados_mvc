<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/04/18
 * Time: 15:09
 */

namespace controllers;


use core\Controller;
use models\Anuncios;
use models\Categorias;

class AnunciosController extends Controller
{
    function index(): void
    {
        $this->validaLogin();

        $a = new Anuncios();
        $anuncios = $a->getMeusAnuncios();

        $dados['anuncios'] = $anuncios;

        $this->loadTemplate('anuncios/index', $dados);
    }

    function inserir()
    {
        $this->validaLogin();

        $a = new Anuncios();

        if(isset($_POST['titulo']) && !empty($_POST['titulo'])) {

            $titulo = addslashes($_POST['titulo']);
            $categoria = addslashes($_POST['categoria']);
            $valor = addslashes($_POST['valor']);
            $descricao = addslashes($_POST['descricao']);
            $estado = addslashes($_POST['estado']);

            $a->addAnuncio($titulo, $categoria, $valor, $descricao, $estado);

            $this->flashMessage('success', 'Produto adicionado com sucesso');

        }

        $c = new Categorias();
        $cats = $c->getLista();

        $dados['cats'] = $cats;

        $this->loadTemplate('anuncios/inserir', $dados);
    }

    function excluir($id)
    {
        $this->validaLogin();

        $a = new Anuncios();

        if(isset($id) && !empty($id)) {
            $a->excluirAnuncio($id);
        }

        header('Location: ' . BASE_URL . 'anuncios');
        exit;
    }

    function excluirFoto($id)
    {

        $this->validaLogin();

        $a = new Anuncios();

        if(isset($id) && !empty($id)) {
            $id_anuncio = $a->excluirFoto($id);
        }

        if(isset($id_anuncio)) {
            header('Location: ' . BASE_URL . 'anuncios/editar/' . $id_anuncio);
        } else {
            header('Location: ' . BASE_URL . 'anuncios');
        }

    }

    function editar($id)
    {
        $this->validaLogin();

        $a = new Anuncios();

        $c = new Categorias();
        $cats = $c->getLista();

        if(isset($_POST['titulo']) && !empty($_POST['titulo'])) {

            $titulo = addslashes($_POST['titulo']);
            $categoria = addslashes($_POST['categoria']);
            $valor = addslashes($_POST['valor']);
            $descricao = addslashes($_POST['descricao']);
            $estado = addslashes($_POST['estado']);
            if (isset($_FILES['fotos'])) {
                $fotos = $_FILES['fotos'];
            } else {
                $fotos = array();
            }

            $a->editAnuncio($titulo, $categoria, $valor, $descricao, $estado, $fotos, $id);

            $this->flashMessage('success', 'Produto editado com sucesso');
        }

        if(isset($id) && !empty($id)) {
            $info = $a->getAnuncio($id);
        } else {
            header('Location ' . BASE_URL);
            exit;
        }

        $dados['info'] = $info;
        $dados['cats'] = $cats;

        $this->loadTemplate('anuncios/editar', $dados);
    }

}