<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/04/18
 * Time: 14:18
 */

namespace controllers;


use core\Controller;
use models\Usuarios;

class LoginController extends Controller
{
    function index()
    {
        $u = new Usuarios();
        if(isset($_POST['email']) && !empty($_POST['email'])) {
            $email = addslashes($_POST['email']);
            $senha = $_POST['senha'];

            if($u->login($email, $senha)) {
                header('Location: ' . BASE_URL);
                exit;
            } else {
                $this->flashMessage('danger', 'usuário ou senha incorretos');
            }
        }

        $this->loadTemplate('login/index');
    }

    function sair()
    {
        unset($_SESSION['cLogin']);
        header("Location: " . BASE_URL);
    }

}