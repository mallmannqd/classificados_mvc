<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/04/18
 * Time: 11:56
 */

namespace controllers;


use core\Controller;
use models\Anuncios;

class ProdutoController extends Controller
{
    function abrir($id)
    {
        $a = new Anuncios();

        if (empty($id)){
            header('Location '. BASE_URL);
            exit;
        }

        $info = $a->getAnuncio($id);

        $dados['info'] = $info;

        $this->loadTemplate('produto/index', $dados);
    }

}