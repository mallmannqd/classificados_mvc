<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 13/04/18
 * Time: 09:50
 */

namespace controllers;


use core\Controller;
use models\Usuarios;

class CadastroController extends Controller
{
    public function index()
    {
        $u = new Usuarios();

        if(isset($_POST['nome']) && !empty($_POST['nome'])) {
            $nome = addslashes($_POST['nome']);
            $email = addslashes($_POST['email']);
            $senha = $_POST['senha'];
            $telefone = addslashes($_POST['telefone']);

            if(!empty($nome) && !empty($email) && !empty($senha)) {
                if($u->cadastrar($nome, $email, $senha, $telefone)) {
                    $this->flashMessage('success', "Parabéns! <strong>Cadastrado com sucesso. <a href=\"" . BASE_URL . "login\" class='alert-link'>Faça o login agora</a></strong>");
                } else {
                    $this->flashMessage('warning', "Este usuário já existe! <a href=\"" . BASE_URL."login\" class='alert-link'>Faça o login agora</a>");
                }
            } else {
                $this->flashMessage('warning', 'Preencha todos os campos!');
            }

        }

        $this->loadTemplate('cadastro/index');
    }

}