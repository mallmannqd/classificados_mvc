<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/04/18
 * Time: 09:53
 */

namespace models;


use core\Model;

class Categorias extends Model
{
    public function getLista() {
        $array = array();

        $sql = $this->db->query("SELECT * FROM categorias");
        if($sql->rowCount() > 0) {
            $array = $sql->fetchAll();
        }

        return $array;
    }

}