<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 08/04/18
 * Time: 14:10
 */
define('ENVIRONMENT', 'development');
//define('ENVIRONMENT', 'production');

if (ENVIRONMENT == 'development') {
    ini_set('display_errors', on);
    define('BASE_URL', 'http://localhost/');
    define('FULL_PATH', '/app/');
    define('DBNAME', 'app');
    define('DBHOST', '192.168.0.3');
    define('DBUSER', 'root');
    define('DBPASS', '');
}else {
    define('BASE_URL', 'http://myapp.com.br/');
    define('DBNAME', 'app');
    define('DBHOST', 'mysql.myapp.com.br');
    define('DBUSER', 'myapp');
    define('DBPASS', 'supersecrectfuckingawesomepasswordcrypted');
}